# Codes implementing an explicit temperature coupling in phase-field crystal models of solidification

Openly available Matlab simulation files used to produce the results published in ["Explicit temperature coupling in phase-field crystal models of solidification"](https://arxiv.org/abs/2205.14998)

arXiv: ["2205.14998"](https://arxiv.org/abs/2205.14998)

Published Version: ["Modelling Simul. Mater. Sci. Eng. 30 074004 (2022)"](https://iopscience.iop.org/article/10.1088/1361-651X/ac8abd)

Files are named by the corresponding figures.

## Fig 2

"fig2.m" represents the implementation of the Eshelby inclusion problem, numerically solved by a PFC approach with an a posteriori calculation of the mechanical stress field.


## Fig 3

"fig3a.m" represents the implementation of a growing dendrite and the corresponding temperature field while considering a constant lattice spacing. The same holds for "fig3.m" with an adapting lattice spacing.


## Fig 6

"fig6.m" gives the initial condition (arbitrary positioned and rotated crystal seeds) for the simulation of a dendritic merging.


## Other simulations

The remaining simulations for ["Explicit temperature coupling in phase-field crystal models of solidification"](https://arxiv.org/abs/2205.14998) can be easily verified by adapting the model/material parameter and the domain of definition in "fig2.m", "fig3a.m", "fig3b.m" and "fig6.m".

## Disclaimer

The software is released here under the MIT license. We kindly ask to refer to/cite the related publication ["Explicit temperature coupling in phase-field crystal models of solidification"](https://arxiv.org/abs/2205.14998) for any usage and extension. The authors are thankful for any advice considering typos, mistakes, and/or discussions around the code/implementation or the topic of the related publication in general. Please do not hesitate to contact the main author, Maik Punke, via: maik.punke@tu-dresden.de

