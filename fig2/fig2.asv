%% Figure 2: Eshelby inclusion problem
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% spatial discretization
% discrete physical space
p = 4*pi/sqrt(3);
UC = 100;
L1 = 1.0*UC*p;
L2 = 1.0*UC*sqrt(3)*0.5*p;
dx = p/10;
dy= sqrt(3)*0.5*p/10;
nx = round((L1)/dx);
ny = round((L2)/dy);
x = linspace(dx,L1,nx);
y = linspace(dy,L2,ny)';

% discrete Fourier space
kV = [0:nx-1];
lV = [0:ny-1]';
kV(floor(nx/2)+2:end) = kV(floor(nx/2)+2:end) -nx;
lV(floor(ny/2)+2:end) = lV(floor(ny/2)+2:end) -ny;
imag = 2*pi*sqrt(-1);
kl2M = (kV/L1).^2 + (lV/L2).^2;
kl4M = (kV/L1).^4 + (lV/L2).^4 + 2*(kV/L1).^2.*(lV/L2).^2;
kl6M = (kV/L1).^6 + (lV/L2).^6 + 3*(kV/L1).^2.*(lV/L2).^4 + 3*(kV/L1).^4.*(lV/L2).^2;


%% time discretization
tEnd =100000;
deltatEuler =  5;
C =-0.149;
K = 0.990103902232335;
tStepEuler =  floor(tEnd*K/deltatEuler);


%% model/material parameters
pre = -1/2;
delta = 1;
c1 = 1;
c2 = 1;
c3 = 16;
c4 = -20;
b1 = 1;
b2 = 1;
b3 = 1;
kappa = 1;
lambda = 1.04;
A = 0;
A2 = A;

BPlus = ((2*delta-4*A)*c1+2*sqrt(delta^2 *c2+c3*delta*A+c4*(lambda-kappa)-c3*A.^2))/5;
BMinus = ((2*delta-4*A)*c1-2*sqrt(delta^2 *c2+c3*delta*A+c4*(lambda-kappa)-c3*A.^2))/5;
B = BMinus;
B = BPlus;

R = 10*p;
signedDist = ((x-L1/2).^2+(y-L2/2).^2).^(1/2) -R;
epsilon = p;
chi = 1/2 * (1-tanh(signedDist/epsilon));
epsilonStar = 0.01;
beta = 1-(1- 1/(1+epsilonStar))*chi;
betaSquare = beta.^2;
pAdapt = p./beta;
psi0M = A+B*(cos(2*pi*(x-L1/2)./pAdapt).*cos(2*pi*(y-L2/2)./(sqrt(3)*pAdapt))-pre*cos(4*pi*(y-L2/2)./(sqrt(3)*pAdapt)));

%% integration of evolution equation
Nonl = @(psiF,betaS) -kappa*imag^2*kl2M.*fft2((1-betaS.^2).*ifft2(psiF))-1/2*kl2M*imag^2.*fft2((ifft2(psiF)).^2)+1/3*kl2M*imag^2.*fft2((ifft2(psiF)).^3)...
    +kappa*imag^2*kl2M.*fft2(betaS .*ifft2(imag.^2 *kl2M.*psiF))+kappa*imag.^4*kl4M.*fft2(betaS .*ifft2(psiF))...
    -kl2M*imag^2*C.*psiF;

psiOld = fft2(psi0M);

for s = 1:tStepEuler
    nonEx = Nonl(psiOld,betaSquare);
    psiNew = (psiOld+deltatEuler*nonEx)./(1-deltatEuler*...
        ((lambda + C)*kl2M*imag^2+ imag^6.*kl6M.*kappa));
    psiOld = psiNew;
end

psiFSol = psiNew;
psiSol = real(ifft2(psiFSol));

%% Cauchy stress tensor
% 1 mode approx
ref = A+B*(cos(2*pi*(x-L1/2)./p).*cos(2*pi*(y-L2/2)./(sqrt(3)*p))-pre*cos(4*pi*(y-L2/2)./(sqrt(3)*p)));
ref = fft2(ref);
[s2k,s2l,s4Ck,s4Cl,s3k,s3l,s3Ck,s3Cl,s4k,s4l,s2Ck,s2Cl] = spots(ref,nx,ny);
[eta0,eta2,phi2,kq2,lq2,eta2C,phi2C,kq2C,lq2C,eta3,phi3,kq3,lq3,eta3C,phi3C,kq3C,lq3C,eta4,phi4,kq4,lq4,eta4C,phi4C,kq4C,lq4C] = phasesVectors(psiSol,kV,lV,imag,4.*pi/sqrt(3),L1,L2,x,y,s2k,s2l,s2Ck,s2Cl,s3k,s3l,s3Ck,s3Cl,s4k,s4l,s4Ck,s4Cl);
psi1Mode = eta0+real(eta2.*(exp(1).^(imag*(kq2*x/L1 + lq2*y/L2)))+ ...
    eta2C.*(exp(1).^(imag*(kq2C*x/L1 + lq2C*y/L2)))+ ...
    eta3.*(exp(1).^(imag*(kq3*x/L1 + lq3*y/L2)))+ ...
    eta3C.*(exp(1).^(imag*(kq3C*x/L1 + lq3C*y/L2)))+ ...
    eta4.*(exp(1).^(imag*(kq4*x/L1 + lq4*y/L2)))+ ...
    eta4C.*(exp(1).^(imag*(kq4C*x/L1 + lq4C*y/L2))));
[xx,yy,xy,ff] = cauchyStress(psi1Mode,imag,kl2M,kl4M,kV,lV,L1,L2,betaSquare,p,kappa,lambda);

%% functions
function [xx,yy,xy,ff] = cauchyStress(psiSol,imag,kl2M,kl4M,kV,lV,L1,L2,betaSquare,p,kappa,lambda)

psiHessXX = ifft2(imag^2*(kV/L1).^2 .*fft2(psiSol));
psiHessYY = ifft2(imag^2*(lV/L2).^2 .*fft2(psiSol));
psiHessXY = ifft2(imag^2*(kV/L1).*(lV/L2).*fft2(psiSol));
sigmaXXV = -kappa*(ifft2(imag^2 *kl2M.*fft2(psiSol))).*psiHessXX;
sigmaYYV = -kappa*( ifft2(imag^2 *kl2M.*fft2(psiSol))).*psiHessYY;
sigmaXYV = -kappa*(ifft2(imag^2 *kl2M.*fft2(psiSol))).*psiHessXY;
gradPsiX = imag.*ifft2(kV/L1 .*fft2(psiSol));
gradPsiY = imag.*ifft2(lV/L2 .*fft2(psiSol));
otherTermX = imag.*ifft2(kV/L1 .*(fft2(betaSquare) + (kl2M.*imag.^2.*fft2(psiSol))));
otherTermY = imag.*ifft2(lV/L2 .*(fft2(betaSquare) + (kl2M.*imag.^2.*fft2(psiSol))));
sigmaXX = otherTermX.*gradPsiX+2*betaSquare .*gradPsiX.*gradPsiX;
sigmaXY = otherTermX.*gradPsiY+2*betaSquare .*gradPsiX.*gradPsiY;
sigmaYX = otherTermY.*gradPsiX+2*betaSquare .*gradPsiY.*gradPsiX;
sigmaYY = otherTermY.*gradPsiY+2*betaSquare .*gradPsiY.*gradPsiY;
sigmaXX = kappa*sigmaXX;
sigmaXY = kappa*sigmaXY;
sigmaYY = kappa*sigmaYY;

% isotropic pressure term
f = 0.5*(lambda-kappa*(1-betaSquare.^2)).*psiSol.^2-1/6*psiSol.^3+1/12 *psiSol.^4 ...
    +kappa*0.5*(-2*(gradPsiX.*(imag.*ifft2(kV/L1 .*(fft2(betaSquare.*psiSol))))+gradPsiY.*(imag.*ifft2(lV/L2 .*(fft2(betaSquare.*psiSol))))) ...
    +(ifft2(imag^2 *kl2M.*fft2(psiSol))).^2);
fVar = (lambda-kappa*(1-betaSquare.^2)).*psiSol-1/2*psiSol.^2+1/3 *psiSol.^3 ...
    +kappa*betaSquare.*ifft2(imag^2 *kl2M.*fft2(psiSol))+kappa*ifft2(imag^2 *kl2M.*fft2(betaSquare.*psiSol))+kappa*ifft2(imag^4 *kl4M.*fft2(psiSol));
f = f-psiSol.*fVar;

% preparation for coarse graining
at = p;
kernel = exp(1).^((-2*pi^2*(at^2*(kV/L1).^2+(sqrt(3)*at*0.5)^2*(lV/L2).^2)));

% coarse grain f
t = fft2(f);
t = t.*kernel;
t = ifft2(t);
ff = real(t);
% coarse grain sigma_xx
t = fft2(sigmaXX+sigmaXXV);
t = t.*kernel;
t = ifft2(t);
xx = real(t);
% coarse grain sigma_yy
t = fft2(sigmaYY+sigmaYYV);
t = t.*kernel;
t = ifft2(t);
yy = real(t);
% coarse grain sigma_xy
t = fft2(sigmaXY+sigmaXYV);
t = t.*kernel;
t = ifft2(t);
xy = real(t);
end


function [s2k,s2l,s2Ck,s2Cl,s3k,s3l,s3Ck,s3Cl,s4k,s4l,s4Ck,s4Cl] = spots(temp,nx,ny)
%spot3
[~,s3k] = max(abs(temp(2:floor(end/2),1)));
% if Omega is multiple of UC (Omega = c*UC) than s3K = c+1
s3k = s3k+1;
s3l = 1;

%spot3 complex conjugate
s3Ck = ny-s3k+2;
s3Cl = s3l;

%spot 2: middle spot
s2k = round(s3k/2);
s2l = s3k;

%spot2 complex conjugate
s2Ck = ny-s2k+2;
s2Cl = s2l;

%spot 4: middle spot
s4k = s2k;
s4l = nx-s2l+2;

%spot4 complex conjugate
s4Ck = ny-s4k+2;
s4Cl = s4l;

end


function [eta0,eta2,phi2,kq2,lq2,eta2C,phi2C,kq2C,lq2C,eta3,phi3,kq3,lq3,eta3C,phi3C,kq3C,lq3C,eta4,phi4,kq4,lq4,eta4C,phi4C,kq4C,lq4C] = phasesVectors(psiSol,kV,lV,imag,p,L1,L2,x,y,s2k,s2l,s2Ck,s2Cl,s3k,s3l,s3Ck,s3Cl,s4k,s4l,s4Ck,s4Cl)
[eta0,~,~] = amplitudes(psiSol,kV,lV,imag,1,1,p,L1,L2,x,y);
[eta3,kq3,lq3] = amplitudes(psiSol,kV,lV,imag,s3k,s3l,p,L1,L2,x,y);
% if sign(max(max(real(eta3))))-sign(min(min(real(eta3)))) >0.5
%     sig = -sign(mean(mean(eta3)));
%     maxV = max(max(real(sig.*eta3)));
%     eta3= eta3-maxV;
% end
phi3 = (angle(eta3));
%phi3(phi3<0)=2*pi+phi3(phi3<0);



[eta3C,kq3C,lq3C] = amplitudes(psiSol,kV,lV,imag,s3Ck,s3Cl,p,L1,L2,x,y);
% if sign(max(max(real(eta3C))))-sign(min(min(real(eta3C)))) >0.5
%     sig = -sign(mean(mean(eta3C)));
%     maxV = max(max(real(sig.*eta3C)));
%     eta3C= eta3C-maxV;
% end
phi3C =(angle(eta3C));
%phi3C(phi3C<0)=2*pi+phi3C(phi3C<0);

[eta2,kq2,lq2] = amplitudes(psiSol,kV,lV,imag,s2k,s2l,p,L1,L2,x,y);
% if sign(max(max(real(eta2))))-sign(min(min(real(eta2)))) >0.5
%     sig = -sign(mean(mean(eta2)));
%     maxV = max(max(real(sig.*eta2)));
%     eta2= eta2-maxV;
% end
phi2 = (angle(eta2));
%phi2(phi2<0)=2*pi+phi2(phi2<0);

[eta2C,kq2C,lq2C] = amplitudes(psiSol,kV,lV,imag,s2Ck,s2Cl,p,L1,L2,x,y);
% if sign(max(max(real(eta2C))))-sign(min(min(real(eta2C)))) >0.5
%     sig = -sign(mean(mean(eta2C)));
%     maxV = max(max(real(sig.*eta2C)));
%     eta2C= eta2C-maxV;
% end
phi2C = (angle(eta2C));
%phi2C(phi2C<0)=2*pi+phi2C(phi2C<0);

[eta4,kq4,lq4] = amplitudes(psiSol,kV,lV,imag,s4k,s4l,p,L1,L2,x,y);
phi4 = (angle(eta4));
[eta4C,kq4C,lq4C] = amplitudes(psiSol,kV,lV,imag,s4Ck,s4Cl,p,L1,L2,x,y);
phi4C =(angle(eta4C));

end