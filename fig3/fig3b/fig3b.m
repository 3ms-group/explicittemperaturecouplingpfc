%% Figure 3a: dendritic growth variable lattice spacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% spatial discretization
% discrete physical space
nx = 2^11;
ny = nx;
p = 4*pi/sqrt(3);
L1 = 1600;
L2 = L1;
dx = L1/nx;
dy = L2/ny;
x = linspace(0,L1,nx);
y = linspace(0,L2,ny)';

% discrete Fourier space
kV = [0:nx-1];
lV = [0:ny-1]';
kV(floor(nx/2)+2:end) = kV(floor(nx/2)+2:end) -nx;
lV(floor(ny/2)+2:end) = lV(floor(ny/2)+2:end) -ny;
imag = 2*pi*sqrt(-1);
kl2M = (kV/L1).^2 + (lV/L2).^2;
kl4M = (kV/L1).^4 + (lV/L2).^4 + 2*(kV/L1).^2.*(lV/L2).^2;
kl6M = (kV/L1).^6 + (lV/L2).^6 + 3*(kV/L1).^2.*(lV/L2).^4 + 3*(kV/L1).^4.*(lV/L2).^2;

%% time discretization
tEnd = 35000;
deltatEuler =  5;
C =-0.149;
K = 0.990103902232335;
tStepEuler = 8;floor(tEnd*K/deltatEuler);

%% model/material parameters
pre = -1/2;
delta = 1;
c1 = 1;
c2 = 1;
c3 = 16;
c4 = -20;
b1 = 1;
b2 = 1;
b3 = 1;
alphaTherm =1.3;
gamma = 0.06;
thetaDivGamma = 0.1/0.06;
MDivTheta = 0.1/0.1;
theta = gamma*thetaDivGamma;
kappa = 0.459810;
lambda = 0.6;
M = MDivTheta*theta;
A = 0.849;
A2 = A;
BPlus = ((2*delta-4*A)*c1+2*sqrt(delta^2 *c2+c3*delta*A+c4*(lambda-kappa)-c3*A.^2))/5;
BMinus = ((2*delta-4*A)*c1-2*sqrt(delta^2 *c2+c3*delta*A+c4*(lambda-kappa)-c3*A.^2))/5;
B = BMinus;

T0 =0.6;
T0M = T0*ones(length(x),length(y));

R2 = 6*p^2;

psi0M = A+B*(cos(2*pi*(x-L1/2)/p).*cos(2*pi*(y-L2/2)/(sqrt(3)*p))-pre*cos(4*pi*(y-L1/2)/(sqrt(3)*p)));
psi0M((x-L1/2).^2+(y-L2/2).^2>=R2) = A2;

freeE = zeros(tStepEuler,1);
lD = zeros(tStepEuler,1);

psi0MF = fft2(psi0M);
T0MF = fft2(T0M);

%% integration of evolution equation
% free energy
F = @(psi,psiF,T,betaSquare) dy*trapz(dx*trapz(real((lambda-kappa*(1-betaSquare.^2))./2 .*T.*psi.^2-T/6.*psi.^3+T/12.*psi.^4 ...
    -gamma*(psi+1)-alpha*T.*log(T)+kappa*T/2.*(-2*(ifft2(imag.*kV./L1 .*psiF).*(ifft2(imag.*kV./L1 .*fft2(betaSquare.*psi)))...
    +ifft2(imag.*lV./L2 .*psiF).*(ifft2(imag.*lV./L2 .*fft2(betaSquare.*psi))))+(ifft2(kl2M.*imag^2.*psiF)).^2)),2),1);

Nonl = @(TF,psiF,betaS) -kappa*imag^2*kl2M.*fft2((1-betaS.^2).*ifft2(psiF))-1/2*kl2M*imag^2.*fft2((ifft2(psiF)).^2)+1/3*kl2M*imag^2.*fft2((ifft2(psiF)).^3)...
    +kappa*imag^2*kl2M.*fft2(betaS .*ifft2(imag.^2 *kl2M.*psiF))+kappa*imag.^4*kl4M.*fft2(betaS .*ifft2(psiF))...
    -gamma*kl2M*imag^2.*fft2((ifft2(TF).^(-1)))-kl2M*imag^2*C.*psiF;

psiOld = psi0MF;
TOld = T0MF;

for s =1:tStepEuler
    betaSquare = (1+alphaTherm*(ifft2(TOld)-T0)).^(-2);
    nonEx = Nonl(TOld,psiOld,betaSquare);
    % update psi
    psiNew = (psiOld+deltatEuler*nonEx)./(1-deltatEuler*...
        ((lambda + C)*kl2M*imag^2+ imag^6.*kl6M.*kappa));
    % update T
    TNew = (TOld+gamma/theta*deltatEuler*(nonEx+...
        ((lambda+C)*kl2M*imag^2+ imag^6.*kl6M.*kappa).*psiNew))./(1-...
        deltatEuler/theta*M.*imag^2.*kl2M);
    
    TOld = TNew;
    psiOld = psiNew;
    
    %postprocessing
    psiSol = real(ifft2(psiNew));
    psi = psiSol;
    maxPsi = max(max(psi));
    psi(islocalmax(-psiSol)) = 0;
    meanMaxPsi = mean([mean(mean(psiSol)),maxPsi]);
    psi(psi<meanMaxPsi) = 0;
    m = islocalmax(psiSol);
    n = logical(psiSol>=meanMaxPsi);
    mn = logical(m.*n);
    [X,Y] = meshgrid(x,y);
    X = X(mn);
    Y = Y(mn);
    dist = (X-L1/2).^2+(Y-L2/2).^2;
    dist = sqrt(max(dist));
    lD(s) = dist;
    
end

TFSol = TNew;
TSol = real(ifft2(TFSol));
psiFSol = psiNew;
psiSol = real(ifft2(psiFSol));



% 1 mode approx
ref = A+B*(cos(2*pi*(x-L1/2)./p).*cos(2*pi*(y-L2/2)./(sqrt(3)*p))-pre*cos(4*pi*(y-L2/2)./(sqrt(3)*p)));
ref = fft2(ref);
[s2k,s2l,s4Ck,s4Cl,s3k,s3l,s3Ck,s3Cl,s4k,s4l,s2Ck,s2Cl] = spots(ref,nx,ny);
[eta0,eta2,phi2,kq2,lq2,eta2C,phi2C,kq2C,lq2C,eta3,phi3,kq3,lq3,eta3C,phi3C,kq3C,lq3C,eta4,phi4,kq4,lq4,eta4C,phi4C,kq4C,lq4C] = phasesVectors(psiSol,kV,lV,imag,4.*pi/sqrt(3),L1,L2,x,y,s2k,s2l,s2Ck,s2Cl,s3k,s3l,s3Ck,s3Cl,s4k,s4l,s4Ck,s4Cl);
psi1Mode = eta0+real(eta2.*(exp(1).^(imag*(kq2*x/L1 + lq2*y/L2)))+ ...
    eta2C.*(exp(1).^(imag*(kq2C*x/L1 + lq2C*y/L2)))+ ...
    eta3.*(exp(1).^(imag*(kq3*x/L1 + lq3*y/L2)))+ ...
    eta3C.*(exp(1).^(imag*(kq3C*x/L1 + lq3C*y/L2)))+ ...
    eta4.*(exp(1).^(imag*(kq4*x/L1 + lq4*y/L2)))+ ...
    eta4C.*(exp(1).^(imag*(kq4C*x/L1 + lq4C*y/L2))));

function [eta0,eta2,phi2,kq2,lq2,eta2C,phi2C,kq2C,lq2C,eta3,phi3,kq3,lq3,eta3C,phi3C,kq3C,lq3C,eta4,phi4,kq4,lq4,eta4C,phi4C,kq4C,lq4C] = phasesVectors(psiSol,kV,lV,imag,p,L1,L2,x,y,s2k,s2l,s2Ck,s2Cl,s3k,s3l,s3Ck,s3Cl,s4k,s4l,s4Ck,s4Cl)
[eta0,~,~] = amplitudes(psiSol,kV,lV,imag,1,1,p,L1,L2,x,y);
[eta3,kq3,lq3] = amplitudes(psiSol,kV,lV,imag,s3k,s3l,p,L1,L2,x,y);
phi3 = (angle(eta3));
[eta3C,kq3C,lq3C] = amplitudes(psiSol,kV,lV,imag,s3Ck,s3Cl,p,L1,L2,x,y);
phi3C =(angle(eta3C));
[eta2,kq2,lq2] = amplitudes(psiSol,kV,lV,imag,s2k,s2l,p,L1,L2,x,y);
phi2 = (angle(eta2));
[eta2C,kq2C,lq2C] = amplitudes(psiSol,kV,lV,imag,s2Ck,s2Cl,p,L1,L2,x,y);
phi2C = (angle(eta2C));
[eta4,kq4,lq4] = amplitudes(psiSol,kV,lV,imag,s4k,s4l,p,L1,L2,x,y);
phi4 = (angle(eta4));
[eta4C,kq4C,lq4C] = amplitudes(psiSol,kV,lV,imag,s4Ck,s4Cl,p,L1,L2,x,y);
phi4C =(angle(eta4C));
end


function  [t,kq,lq] = amplitudes(psi,kV,lV,imag,qx,qy,p,L1,L2,x,y)
kq = kV(qy);
lq = lV(qx);
at = 1*p;
kernel = exp(1).^(-2*pi^2*(at^2*((kV-kq)/L1).^2+(sqrt(3)*0.5*at)^2*((lV-lq)/L2).^2));
t = fft2(psi);
t = t.*kernel;
t = ifft2(t);
t = t.*(exp(1).^(-imag*(kq*x/L1 + lq*y/L2)));
end