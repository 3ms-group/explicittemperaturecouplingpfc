%% Figure 6: dendritic merging, initial conditions

%% spatial discretization
% discrete physical space
nx = 2^12;
ny = nx;
p = 4*pi/sqrt(3);
L1 = 3200;
L2 = L1;
dx = L1/nx;
dy = L2/ny;
x = linspace(0,L1,nx);
y = linspace(0,L2,ny)';

% initial condition
alpha3 = -7/360*2*pi;
alpha2 = 15/360*2*pi;
rotMat1 =[cos(alpha3), -sin(alpha3);sin(alpha3),cos(alpha3)];
[xR1,yR1] = meshgrid(x,y);
rotMat2 =[cos(alpha2), -sin(alpha2);sin(alpha2),cos(alpha2)];
[xR2,yR2] = meshgrid(x,y);
xyR1 = [xR1(:) yR1(:)];
xyR1 = xyR1*rotMat1';
xR1 = xyR1(:,1);
yR1 = xyR1(:,2);
xyR2 = [xR2(:) yR2(:)];
xyR2 = xyR2*rotMat2';
xR2 = xyR2(:,1);
yR2 = xyR2(:,2);
px1 = L1/2+L1/10;
py1 = 3*L2/4-L2/20;
px2 = L1/2-L1/10;
py2 = 1*L2/4+L2/20;
pxy1 = [px1,py1]*rotMat1';
px1 = pxy1(1);
py1 = pxy1(2);
pxy2 = [px2,py2]*rotMat2';
px2 = pxy2(1);
py2 = pxy2(2);

psi0M1 = A+B*(cos(2*pi*(xR1-px1)/p).*cos(2*pi*(yR1-py1)/(sqrt(3)*p))-paper*cos(4*pi*(yR1-py1)/(sqrt(3)*p)));
psi0M2 = A+B*(cos(2*pi*(xR2-px2)/p).*cos(2*pi*(yR2-py2)/(sqrt(3)*p))-paper*cos(4*pi*(yR2-py2)/(sqrt(3)*p)));
psi0M1((xR1-px1).^2+(yR1-py1).^2>R2) = 0;
psi0M2((xR2-px2).^2+(yR2-py2).^2>R2) = 0;
psi0M1 = reshape(psi0M1,[nx,ny]);
psi0M2 = reshape(psi0M2,[nx,ny]);
psi0M = psi0M1+psi0M2;
psi0M(psi0M==0)=A2;

% initial temperature
T0 =0.6;
T0M = T0*ones(length(y),length(x));

